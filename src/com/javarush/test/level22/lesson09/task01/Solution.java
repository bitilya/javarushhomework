package com.javarush.test.level22.lesson09.task01;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* Обращенные слова
В методе main с консоли считать имя файла, который содержит слова, разделенные пробелами.
Найти в тексте все пары слов, которые являются обращением друг друга. Добавить их в result.
Порядок слов first/second не влияет на тестирование.
Использовать StringBuilder.
Пример, "мор"-"ром", "трос"-"сорт"
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args)throws Exception{
        BufferedReader red = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream red2 = new FileInputStream(red.readLine());
        byte[] arr = new byte[red2.available()];
        while(red2.available()>0){red2.read(arr);}
        String ress = new String(arr);
        String[] ss = ress.split(" ");
        ArrayList<String> as = new ArrayList<String>();
        for(String h :ss)
        {
            as.add(h);
        }
        int x=0;
        StringBuilder b = new StringBuilder(ss[1]);

       loop: for(int i=0;i<ss.length-1;i++)
        {
            StringBuilder bil = new StringBuilder(ss[i]);
            System.out.println(bil.toString());
            for(int j=i+1-x;j<as.size();j++)
            {
                if(bil.reverse().toString().equals(ss[j])){System.out.println(as.get(j));result.add(new Pair(ss[i], as.get(j)));as.remove(j);x++;continue loop;}
            }
        }
       // for(Pair ase : result)
     //   {
      //      System.out.println(ase);
      //  }

    }

    public static class Pair {
        String first;
        String second;
        public Pair(String a,String b)
        {
            first=a;second=b;
        }

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null && second != null ? second :
                    second == null && first != null ? first :
                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}

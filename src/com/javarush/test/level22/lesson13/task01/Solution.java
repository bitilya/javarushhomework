package com.javarush.test.level22.lesson13.task01;

import java.util.Arrays;
import java.util.StringTokenizer;

/* StringTokenizer
Используя StringTokenizer разделить query на части по разделителю delimiter.
Пример,
getTokens("level22.lesson13.task01", ".") == {"level22", "lesson13", "task01"}
*/
public class Solution {
    public static String [] getTokens(String query, String delimiter) {
        StringTokenizer st = new StringTokenizer(query,delimiter);
        String[] pop=new String[0];
        int i=1;
        while(st.hasMoreTokens())
        {
          String[]  bob = Arrays.copyOf(pop,i);
            bob[i-1]=st.nextToken();
            pop=bob;
            i++;
        }
        return pop;
    }
    public static void main(String...ar)
    {
        String a = "sasa.sasa.sssss.ddddd.";
        String aa=".";
        String[] test = getTokens(a,aa);
        for(String q:test){System.out.println(q);}
    }
}

package com.javarush.test.level22.lesson13.task02;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

/* Смена кодировки
В метод main первым параметром приходит имя файла, тело которого в кодировке Windows-1251.
В метод main вторым параметром приходит имя файла, в который необходимо записать содержимое первого файла в кодировке UTF-8.
*/
public class Solution {
    static String win1251TestString = "РќР°СЂСѓС€РµРЅРёРµ РєРѕРґРёСЂРѕРІРєРё РєРѕРЅСЃРѕР»Рё?"; //only for your testing

    public static void main(String[] args) throws IOException {

        FileInputStream fr = new FileInputStream(args[0]);
        FileOutputStream wr = new FileOutputStream(args[1]);
        byte[] arr = new byte[fr.available()];
        while(fr.available()>0)
        {
            fr.read(arr);
        }
        fr.close();

        String a = new String(arr,"UTF-8");
        byte[] slon = a.getBytes("Windows-1251");
        wr.write(slon);
        wr.close();
    }
}

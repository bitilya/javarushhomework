package com.javarush.test.level20.lesson02.task03;


import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
/* Знакомство с properties
В методе fillInPropertiesMap считайте имя файла с консоли и заполните карту properties данными из файла.
Про .properties почитать тут - http://ru.wikipedia.org/wiki/.properties
Реализуйте логику записи в файл и чтения из файла для карты properties.
*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();

    public void fillInPropertiesMap() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        InputStream is = null;
        try {
            String str = br.readLine();
            is = new FileInputStream(str);
            load(is);
        }
        catch(Exception exc){
            exc.printStackTrace();
            System.err.println(exc.getMessage());
          }
        finally {
            try {
                br.close();
                is.close();
            }
            catch (IOException d){}
        }
        //implement this method - реализуйте этот метод
    }

    public void save(OutputStream outputStream) throws Exception {
        Properties pr = new Properties();
        for(Map.Entry<String,String> entr : properties.entrySet()){
            pr.setProperty(entr.getKey(),entr.getValue());
        }
        pr.store(outputStream,"");
        //implement this method - реализуйте этот метод
    }

    public void load(InputStream inputStream) throws Exception {
        Properties pr = new Properties();
        pr.load(inputStream);
        Enumeration<?> en = pr.propertyNames();
        for(;en.hasMoreElements();){
            String str = String.valueOf(en.nextElement());
            properties.put(str,pr.getProperty(str));
        }
        //implement this method - реализуйте этот метод
    }

}

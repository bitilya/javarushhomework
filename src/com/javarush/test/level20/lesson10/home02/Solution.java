package com.javarush.test.level20.lesson10.home02;

import java.io.*;

/* Десериализация
На вход подается поток, в который записан сериализованный объект класса A либо класса B.
Десериализуйте объект в методе getOriginalObject предварительно определив, какого именно типа там объект.
Реализуйте интерфейс Serializable где необходимо.
*/
public class Solution implements Serializable{
    public A getOriginalObject(ObjectInputStream objectStream)throws Exception{
        Object a =  objectStream.readObject();System.out.println("deser");
        if (a instanceof B){System.out.println("B");return (B) a;}
        else {System.out.println("A");return (A) a;}

    }

    public  class A implements Serializable {
        public A(){System.out.println("its A    ");}
    }

    public class B extends A implements Serializable{
        public B() {
            System.out.println("inside B");
        }
    }
    public static void main(String[] a)throws Exception
    {
        Solution s = new Solution();
        A b =  s.new A();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        ObjectOutputStream obo = new ObjectOutputStream(bo);
        obo.writeObject(b);System.out.println("Ser");
        ByteArrayInputStream bis = new ByteArrayInputStream(bo.toByteArray());
        ObjectInputStream oi = new ObjectInputStream(bis);
        A asa = s.getOriginalObject(oi);
    }
}

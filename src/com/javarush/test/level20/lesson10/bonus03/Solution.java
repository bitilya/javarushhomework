package com.javarush.test.level20.lesson10.bonus03;

import java.util.ArrayList;
import java.util.List;

/* Кроссворд
1. Дан двумерный массив, который содержит буквы английского алфавита в нижнем регистре.
2. Метод detectAllWords должен найти все слова из words в массиве crossword.
3. Элемент(startX, startY) должен соответствовать первой букве слова, элемент(endX, endY) - последней.
text - это само слово, располагается между начальным и конечным элементами
4. Все слова есть в массиве.
5. Слова могут быть расположены горизонтально, вертикально и по диагонали как в нормальном, так и в обратном порядке.
6. Метод main не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'p', 'd', 'e', 'r', 's', 'k'},
                {'u', 'o', 'a', 'e', 'e', 'o'},
                {'l', 'n', 'p', 'r', 's', 'v'},
                {'m', 'l', 'p', 't', 'r', 'h'},
                {'k', 'o', 'r', 'n', 'o', 't'}
        };
       List<Word> re =  detectAllWords(crossword, "lup", "vok","ht","e");
        for(Word dd : re)
        {
            System.out.println(dd);
        }

        /*
Ожидаемый результат
home - (5, 3) - (2, 0)
same - (1, 1) - (4, 1)
         */
    }

    public static int[] finder(int[][] a,int x, int y,String sl)
    {
        char[] n = new char[sl.length()];
        int i,j,ind;
        int[] res = new int[3];
        i=x;j=y;ind=0;
        while(i>=0 && ind<sl.length()){n[ind++]=(char)a[i--][j];}if(sl.equals(String.valueOf(n))){res[0]=1;res[1]=++i;res[2]=j;return res;}
        ind=0;i=x;j=y;
        while(i>=0 && j<=a[0].length-1 && ind<sl.length()){n[ind++]=(char)a[i--][j++];}if(sl.equals(String.valueOf(n))){res[0]=1;res[1]=++i;res[2]=--j;return res;}
        i=x;j=y;ind=0;
        while(j<=a[0].length-1 && ind<sl.length()){n[ind++]=(char)a[i][j++];}if(sl.equals(String.valueOf(n))){res[0]=1;res[1]=i;res[2]=--j;return res;}
        i=x;j=y;ind=0;
        while(i<=a.length-1 &&j<=a[0].length-1&& ind<sl.length()){n[ind++]=(char)a[i++][j++];}if(sl.equals(String.valueOf(n))){res[0]=1;res[1]=--i;res[2]=--j;return res;}
        i=x;j=y;ind=0;
        while(i<=a.length-1 && ind<sl.length()){n[ind++]=(char)a[i++][j];}if(sl.equals(String.valueOf(n))){res[0]=1;res[1]=--i;res[2]=j;return res;}
        i=x;j=y;ind=0;
        while(i<=a.length-1 && j>=0 && ind<sl.length()){n[ind++]=(char)a[i++][j--];}if(sl.equals(String.valueOf(n))){res[0]=1;res[1]=--i;res[2]=++j;return res;}
        i=x;j=y;ind=0;
        while(j>=0 && ind<sl.length()){n[ind++]=(char)a[i][j--];}if(sl.equals(String.valueOf(n))){res[0]=1;res[1]=i;res[2]=++j;return res;}
        i=x;j=y;ind=0;
        while(i>=0 &&j>=0&& ind<sl.length()){n[ind++]=(char)a[i--][j--];}if(sl.equals(String.valueOf(n))){res[0]=1;res[1]=++i;res[2]=++j;return res;}
        res[0] = 0;return res;
    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {
        List<Word> ls = new ArrayList<Word>();
        String[] povtor = new String[words.length];int o=0;
        loop :for(String s : words)
        {
            for(String ded: povtor){if(s.equals(ded))break loop;}
            char[] cc = s.toCharArray();
            for (int i=0;i<crossword.length;i++)
            {
                for(int j=0;j<crossword[0].length;j++)
                {
                    if(cc[0]==(char)crossword[i][j]){
                        int[] res = finder(crossword,i,j,s);
                        if(res[0]==1)
                        {
                            ls.add(new Word(s));ls.get(ls.size()-1).setStartPoint(j,i);ls.get(ls.size()-1).setEndPoint(res[2],res[1]);
                            povtor[o++] = s;continue loop;
                        }
                    }
                }
            }
        }

        return ls;
    }

    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public void setStartPoint(int i, int j) {
            startX = i;
            startY = j;
        }

        public void setEndPoint(int i, int j) {
            endX = i;
            endY = j;
        }

        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }
}

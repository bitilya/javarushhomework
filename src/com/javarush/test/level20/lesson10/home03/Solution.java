package com.javarush.test.level20.lesson10.home03;

import java.io.*;

/* Найти ошибки
Почему-то при сериализации/десериализации объекта класса B возникают ошибки.
Найдите проблему и исправьте ее.
Класс A не должен реализовывать интерфейсы Serializable и Externalizable.
Сигнатура класса В не содержит ошибку :)
Метод main не участвует в тестировании.
*/
public class Solution implements Serializable {
    public static class A {
        public A(){}
        protected String name = "A";

        public A(String name) {
            this.name += name;
        }
    }

    public class B extends A implements Serializable {
        public B(String name) {
            super(name);
            this.name += name;
        }
        private void writeObject(ObjectOutputStream stream) throws IOException
        {
            stream.writeObject(this.name);
        }
        private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException
        {
            this.name = (String) stream.readObject();

        }
    }
    public static void main(String[] a)throws Exception
    {
        B b = new Solution(). new B("joker");
        FileOutputStream fo = new FileOutputStream("C:\\a.txt");
        ObjectOutputStream obu = new ObjectOutputStream(fo);
        FileInputStream fi = new FileInputStream("C:\\a.txt");
        ObjectInputStream ois = new ObjectInputStream(fi);
        obu.writeObject(b);System.out.println("ser");
        System.out.println(b.name);
        B b1 = (B) ois.readObject();System.out.println("deser");
        System.out.println(b1.name);

    }
}

package com.javarush.test.level20.lesson07.task04;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/* Serializable Solution
Сериализуйте класс Solution.
Подумайте, какие поля не нужно сериализовать, пометить ненужные поля — transient.
Объект всегда должен содержать актуальные на сегодняшний день данные.
Метод main не участвует в тестировании.
Написать код проверки самостоятельно в методе main:
1) создать файл, открыть поток на чтение (input stream) и на запись(output stream)
2) создать экземпляр класса Solution - savedObject
3) записать в поток на запись savedObject (убедитесь, что они там действительно есть)
4) создать другой экземпляр класса Solution с другим параметром
5) загрузить из потока на чтение объект - loadedObject
6) проверить, что savedObject.string равна loadedObject.string
7) обработать исключения
*/
public class Solution implements Serializable{
    public static void main(String[] args) throws Exception{
        System.out.println(new Solution(4));
       // File your_file_name = File.createTempFile("your_file_name", null);
        OutputStream outputStream = new FileOutputStream("C:\\a.txt");
        FileInputStream inputStream = new FileInputStream("C:\\a.txt");

        ObjectOutputStream oou = new ObjectOutputStream(outputStream);
        Solution savedObject = new Solution(1);
        oou.writeObject(savedObject);
        oou.flush();
        Solution loadedObject = new Solution(10);
        ObjectInputStream oi = new ObjectInputStream(inputStream);
        loadedObject = (Solution)oi.readObject();
        inputStream.close();
        oi.close();
        oou.close();
        System.out.println(savedObject.string+" "+loadedObject.string);
    }

    private transient final String pattern = "dd MMMM yyyy, EEEE";
    private transient Date currentDate;
    private transient int temperature;
     String string;

    public Solution(int temperature) {
        this.currentDate = new Date();
        this.temperature = temperature;

        string = "Today is %s, and current temperature is %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);
    }

    @Override
    public String toString() {
        return this.string;
    }
}

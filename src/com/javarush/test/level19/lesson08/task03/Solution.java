package com.javarush.test.level19.lesson08.task03;

/* Выводим только цифры
В методе main подмените объект System.out написанной вами ридер-оберткой по аналогии с лекцией
Ваша ридер-обертка должна выводить только цифры
Вызовите готовый метод printSomething(), воспользуйтесь testString
Верните переменной System.out первоначальный поток
Вывести модифицированную строку в консоль.

Пример вывода:
12345678
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream ps = System.out;
        ByteArrayOutputStream ou = new ByteArrayOutputStream();
        PrintStream pr = new PrintStream(ou);
        System.setOut(pr);
        testString.printSomething();
        System.setOut(ps);
        String s = "wery text ";
        String patt = "(text)";
       Pattern pt = Pattern.compile(patt);
        Matcher m = pt.matcher(s);
        String gg="";
         gg = m.group(0);
        System.out.print(gg);

    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's 1 a 23 text 4 f5-6or7 tes8ting");
        }
    }
}

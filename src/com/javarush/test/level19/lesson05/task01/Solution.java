package com.javarush.test.level19.lesson05.task01;

/* Четные байты
Считать с консоли 2 имени файла.
Вывести во второй файл все байты с четным индексом.
Пример: второй байт, четвертый байт, шестой байт и т.д.
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception{

        BufferedReader red = new BufferedReader(new InputStreamReader(System.in));
        String[] s = new String[2];
        s[0] = red.readLine();s[1]=red.readLine();
        red.close();

        FileInputStream in = new FileInputStream(s[0]);
        FileOutputStream out = new FileOutputStream(s[1]);
        int a =0;
        while(in.available()>0)
        {
            int y = in.read();
            if ((a % 2 == 0) && a!=0)
            {
                out.write(y);
            }
            a++;
        }

    }
}

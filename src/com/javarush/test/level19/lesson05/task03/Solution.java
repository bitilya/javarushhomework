package com.javarush.test.level19.lesson05.task03;

/* Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки. Не использовать try-with-resources

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception{

        BufferedReader red = new BufferedReader(new InputStreamReader(System.in));
        String[] s = new String[2];
        s[0] = red.readLine();s[1] = red.readLine();
        red.close();
        BufferedReader br = new BufferedReader(new FileReader(s[0]));
        ArrayList<String> ar = new ArrayList<String>();
        ArrayList<String> art = new ArrayList<String>();
        String g;
        while((g=br.readLine())!=null)
        {
            ar.add(g);
        }
        for(String str : ar)
        {
            String[] mas = str.split(" ");
            for(int i=0;i<mas.length;i++)
            {
                if(isNumeric(mas[i]))art.add(mas[i]);
            }

        }

        FileWriter fw = new FileWriter(s[1]);
        String wrr;
        for(int i=0;i<art.size();i++)
        {
            wrr=art.get(i);
            if(i!=art.size()-1) wrr=wrr+" ";
            char[] c = wrr.toCharArray();
            fw.write(c);
            fw.flush();
        }
        fw.close();
        br.close();

    }
    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}

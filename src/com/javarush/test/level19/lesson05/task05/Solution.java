package com.javarush.test.level19.lesson05.task05;

/* Пунктуация
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Удалить все знаки пунктуации, вывести во второй файл.
http://ru.wikipedia.org/wiki/%D0%9F%D1%83%D0%BD%D0%BA%D1%82%D1%83%D0%B0%D1%86%D0%B8%D1%8F
Закрыть потоки. Не использовать try-with-resources
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws  Exception{
        BufferedReader red = new BufferedReader(new InputStreamReader(System.in));
        String[] s = new String[2];
        s[0] = red.readLine(); s[1] = red.readLine();
        red.close();

        FileReader fr = new FileReader(s[0]);
        FileWriter fw = new FileWriter(s[1]);
        String str;
        char[] ch = new char[600];
        while(fr.ready())fr.read(ch);
        str = String.valueOf(ch);
        str = str.replaceAll("\\p{Punct}","");
        fw.write(str.toCharArray());
        fr.close();fw.close();
    }
}

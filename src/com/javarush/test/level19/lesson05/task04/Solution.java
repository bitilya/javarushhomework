package com.javarush.test.level19.lesson05.task04;

/* Замена знаков
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Заменить все точки "." на знак "!", вывести во второй файл.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws  Exception{
        BufferedReader red = new BufferedReader(new InputStreamReader(System.in));
        String[] s = new String[2];
        s[0] = red.readLine(); s[1] = red.readLine();
        red.close();

        FileReader fr = new FileReader(s[0]);
        FileWriter fw = new FileWriter(s[1]);
        String str;
        char[] ch = new char[600];
        while(fr.ready())fr.read(ch);
        str=String.valueOf(ch);
        str=str.replace('.','!');
        fw.write(str.toCharArray());
        fr.close();
        fw.close();


    }
}

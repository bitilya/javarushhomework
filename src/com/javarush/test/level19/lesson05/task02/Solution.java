package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Файл содержит слова, разделенные знаками препинания.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception{

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String s = in.readLine();
        FileReader d = new FileReader(s);

        char[] ch = new char[600];
        int saved=0;
        if(d.ready())saved = d.read(ch);
        int count = 0;
        for(int i =0;i<saved-4;i++)
        {
            if(ch[i]=='w'&& ch[i+1]=='o' && ch[i+2]=='r' && ch[i+3]=='l' && ch[i+4]=='d')
            {
                count++;
            }
        }

        System.out.println(count);
        d.close();
        in.close();
    }
}

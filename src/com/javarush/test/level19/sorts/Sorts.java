package com.javarush.test.level19.sorts;

/**
 * Created by qweds on 15.06.16.
 */
public class Sorts
{


    public static void mergeSort(int[] A){
        mergeDiv(A,0,A.length-1);
    }

    private static void mergeDiv(int[] A,int i1,int i2){
        int p =0;
        if(i2 > i1){
            p = (i2 - i1) / 2;
            p +=i1;
            mergeDiv(A,i1,p);
            mergeDiv(A,p+1,i2);
            mergeBuild(A,i1,p,i2);
        }
    }

    private static void mergeBuild(int[] A,int i1,int p,int i2){
        int[] H1,H2;
        int s1,s2 = 0;
        H1 = new int[p-i1+1];
        H2 = new int[i2-p];
        s1 = i1;
        for (int i=0;i<H1.length;i++){
            H1[i] = A[s1++];
        }
        s2 = p+1;
        for (int i = 0;i < H2.length;i++){
            H2[i] = A[s2++];
        }
        s1=0;
        s2=0;
        for(int i = i1;i<=i2;i++){
            if(s1>=H1.length){
                A[i] = H2[s2++];
            }
            else if(s2 >= H2.length){
                A[i] = H1[s1++];
            }
            else if(H2[s2]<H1[s1]){
                A[i] = H2[s2++];
            }
            else A[i] = H1[s1++];
        }
    }

    public static void piramidSort(int[] A) {
        int length = A.length;
        int helpi=0;
        for (int i = A.length/2; i >=0 ; i--) {
            piramidSock(A,i,length);
        }
        for (int i = A.length-1; i >0 ; i--) {
            helpi = A[i];
            A[i] = A[0];
            A[0] = helpi;
            length--;
            piramidSock(A,0,length);
        }

    }

    private static void piramidSock(int[] A,int ind,int length){
        int ch1 = ind*2 + 1;
        int ch2 = ind*2 + 2;
        int changer = 0;
        int helpi=0;
        if(ch1 < length || ch2 < length){
            if(!(ch2 == length)){
                changer = A[ch1] > A[ch2] ? ch1 : ch2;
            }
            else changer = ch1;
            if(A[ind] < A[changer]){
                helpi = A[changer];
                A[changer] = A[ind];
                A[ind] = helpi;
                piramidSock(A,changer,length);
            }
        }
    }

    public static void quickSort(int[] A){
        quickDiv(A,0,A.length-1);

    }

    private static void quickDiv(int[] A,int i1,int i2){
        if(i1 < i2){
            int q = quickFind(A,i1,i2);
            quickDiv(A,i1,q-1);
            quickDiv(A,q+1,i2);
        }
    }

    private static int quickFind(int[] A,int i1,int i2){
        int q =(int) (Math.random()*(i2-i1+1));
        q += i1;
        int changer = 0;
        int i = i1-1;
        for(int j = i1;j <= i2;j++){
            if(A[j] <= A[q]){
                i++;
                changer = A[j];
                A[j] = A[i];
                A[i] = changer;
                if(j == q) q=i;
            }
        }
        changer = A[i];
        A[i] = A[q];
        A[q] = changer;
        return i;
    }


    public static void main(String[] args)
    {
        int[] Z = {65,12,2,-9,0,0,0,-123,1212,Integer.MAX_VALUE,Integer.MIN_VALUE,1,1,-1,-1};
        quickSort(Z);
        for(Integer i : Z){
            System.out.println(i);
        }
    }
}
